import React, { Component } from "react";
import { Link } from "react-router-dom";
import Home from '../pages/Home';

class Header extends Component {
  render() {
    return (
      <div className="super_container">
        <header className="header">
          <div className="container">
            <div className="row">
              <div className="col">
                <div className="header_content d-flex flex-row align-items-center justify-content-start">
                  <div className="logo">
                    <a href="#">avision</a>
                  </div>
                  <nav className="main_nav">
                    <ul>
                      <li className="active">
                        <a href="#">JAVASCRIPT</a>
                      </li>
                      <li>
                        <a href="#">RUBY</a>
                      </li>
                      <li>
                        <a href="#">IOS</a>
                      </li>
                      <li>
                        <a href="#">Lifestyle</a>
                      </li>
                      <li>
                        <a href="#">ANDROID</a>
                      </li>
                      <li>
                        <a href="#">SWIFT</a>
                      </li>
                      <li>
                        <a href="">RAILS</a>
                      </li>
                      <li>
                        <Link to='/login'>LOGIN</Link>
                      </li>
                    </ul>
                  </nav>
                  <div className="search_container ml-auto">
                    <form action="#">
                      <input

                        type="search"
                        className="header_search_input"
                        required="required"
                        placeholder="Type to Search..."
                      />
                      <img
                        className="header_search_icon"
                        src={process.env.PUBLIC_URL + "/assets/search.png"}
                        alt="search_icon"
                      />
                    </form>
                  </div>
                  <div className="hamburger ml-auto menu_mm">
                    <i
                      className="fa fa-bars trans_200 menu_mm"
                      aria-hidden="true"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>

        <Home/>

        
      </div>
    );
  }
}

export default Header;
