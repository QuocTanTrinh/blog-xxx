import React, { Component } from "react";
class Home extends Component {
  constructor(props){
    super(props);
    this.state = {
      postList:[]
    }
  }

  componentDidMount(){
    this.getListPost();
  }
  getListPost = () =>{
    fetch('/posts')
    .then(res=>res.json())
    .then(postList => this.setState({postList}))
  }
  render() {
    const { postList } = this.state;
    const rows = []
    return (
      <div className="Home">
        <h1>List of Posts</h1>
        {/* Check to see if any items are found*/}
        {postList.length ? (
          <div>
            {/* Render the list of items */}
            {postList.map((post) => {
                rows.push(
                  <tr key={post.id}>
                    <td>{post.id}</td>
                    <td>{post.title}</td>
                    <td>{post.content}</td>
                    <td>{post.slug}</td>
                  </tr>
                )
            })}

                  <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Content</th>
                            <th>Slug</th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                  </table>
                
          </div>
        ) : (
          <div>
            <h2>No List Posts Found</h2>
          </div>
        )
      }
      </div>
    );
  }
}

export default Home;
