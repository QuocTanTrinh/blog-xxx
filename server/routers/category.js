var express = require('express');
var router = express.Router();
var CategoryController = require('../controllers/category');

router.get('/', CategoryController.getList);
router.get('/:slug', CategoryController.get);
router.post('/', CategoryController.create);
router.put('/:slug', CategoryController.update);
router.delete('/:slug', CategoryController.delete);
module.exports = router;
