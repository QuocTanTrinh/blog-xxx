var express = require('express');
var router = express.Router();
var PostController = require('../controllers/post');

router.get('/', PostController.getList);
router.get('/:slug', PostController.get);
router.post('/', PostController.create);
router.put('/:slug', PostController.update);
router.delete('/:slug', PostController.delete);

module.exports = router;
