var express = require('express');
var router = express.Router();
var RoleController = require('../controllers/role');

router.get('/', RoleController.getList);
router.get('/:id', RoleController.get);
router.post('/', RoleController.create);
router.put('/:id', RoleController.update);
router.delete('/:id', RoleController.delete);
module.exports = router;
