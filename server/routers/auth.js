var express = require('express');
var router = express.Router();
var AuthController = require('../controllers/auth');

router.post('/token', AuthController.generateToken);

module.exports = router;