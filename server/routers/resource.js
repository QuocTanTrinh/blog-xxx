var express = require('express');
var router = express.Router();
var ResourceController = require('../controllers/resource');

router.get('/', ResourceController.getList);
router.get('/:id', ResourceController.get);
router.post('/', ResourceController.create);
router.put('/:id', ResourceController.update);
router.delete('/:id', ResourceController.delete);
module.exports = router;
