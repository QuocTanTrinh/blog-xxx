var express = require('express');
var router = express.Router();
var UserController = require('../controllers/user');
var JWTMiddleware = require('../middlewares/jwt');
router.get('/', UserController.getList);
router.get('/:id', UserController.get);
router.post('/', UserController.create);
router.put('/:id', UserController.update);
router.delete('/:id', UserController.delete);

module.exports = router;
