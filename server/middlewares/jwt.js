var JWTService = require('../services/jwt');

module.exports = function(req, res, next) {
    var token = req.headers['authorization'];
    if (!token)
        return res.status(401).json({ message: 'Invalid token' });

    JWTService.verify(token)
        .then(function(decoded) {
            console.log(decoded);
            req.id = decoded.id;
            next();
        })
        .catch(function (error) {
            return res.status(401).json(error);
        });
};