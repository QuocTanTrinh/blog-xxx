var UserService = require('../services/user');
var RoleService = require('../services/role');
module.exports.getList = function (req, res) {
    var page = req.query.page || 1;
    return UserService.getList(page)
        .then(function (users) {
            res.json(users);
        })
        .catch(function (error) {
            res.json({message: error})
        });
};

module.exports.get = function (req, res) {
    var id = req.params.id;
    return UserService.getUserDetail(id)
    .then(function (users) {
        res.json(users);
    })
    .catch(function (error) {
        res.json({message: error})
    });
};
module.exports.create = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var avatar = req.body.avatar;
    var fullname = req.body.fullname;
    var dob = req.body.dob;
    var address = req.body.address;
    var email = req.body.email;
    var gender = req.body.gender;
    var phone = req.body.phone;
    var desc = req.body.desc;
    //var roleId = RoleService.getRoleId(req.body.role).id;

    return RoleService.getRoleId(req.body.role)
    .then(function (roles) {
        UserService.createUser(username, password, avatar, fullname, dob, address, email, gender, phone, desc, roles.id)
        res.status(200).json({message: 'Create success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });    
    // return UserService.createUser(username, password, avatar, fullname, dob, address, email, gender, phone, desc, roleId)
    // .then(function (users) {
    //     res.json(users);
    // })
    // .catch(function (error) {
    //     res.json({message: error})
    // });

};


module.exports.update = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var avatar = req.body.avatar;
    var fullname = req.body.fullname;
    var dob = req.body.dob;
    var address = req.body.address;
    var email = req.body.email;
    var gender = req.body.gender;
    var phone = req.body.phone;
    var desc = req.body.desc;
    //var roleId = RoleService.getRoleId(req.body.role).id;

    return UserService.updateUser(username, password, avatar, fullname, dob, address, email, gender, phone, desc)
    .then(function (users) {     
        res.status(200).json({message: 'Updated user success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });    
};

module.exports.delete = function (req, res) {
    var id = req.params.id;
    return UserService.deleteUser(id)
    .then(function (users) {
        res.json({message: 'Deleted user success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });  
}; 
