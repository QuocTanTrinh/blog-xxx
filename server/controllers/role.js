var RoleService = require('../services/role');
module.exports.getList = function (req, res) {
    var page = req.query.page || 1;
    return RoleService.getList(page)
        .then(function (roles) {
            res.json(roles);
        })
        .catch(function (error) {
            res.json({message: error})
        });
};

module.exports.get = function (req, res) {
    var id = req.params.id;
    return RoleService.getRoleDetail(id)
    .then(function (roles) {
        res.json(roles);
    })
    .catch(function (error) {
        res.json({message: error})
    });
};

module.exports.create = function (req, res) {
    var name = req.body.name;
    var desc = req.body.desc;
    return RoleService.createRole(name, desc)
    .then(function (roles) {
        res.json(roles);
    })
    .catch(function (error) {
        res.json({message: error})
    });
};

module.exports.update = function (req, res) {
    var id = req.params.id;
    var name = req.body.name;
    var desc = req.body.desc;
    return RoleService.updateRole(id, name, desc)
    .then(function (roles) {
        res.json(roles);
    })
    .catch(function (error) {
        res.json({message: error})
    });   
};

module.exports.delete = function (req, res) {
    var id = req.params.id;
    return RoleService.deleteRole(id)
    .then(function (roles) {
        res.json(roles);
    })
    .catch(function (error) {
        res.json({message: error})
    });  
};