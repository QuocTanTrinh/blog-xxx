var PostService = require('../services/post');
const slugify = require("slugify");
module.exports.getList = function (req, res) {
    var page = req.query.page || 1;
    return PostService.getList(page)
        .then(function (posts) {
            res.json(posts);
        })
        .catch(function (error) {
            res.json({message: error})
        });
};

module.exports.get = function (req, res) {
    var id = req.params.id;
    return PostService.getPostDetail(id)
    .then(function (posts) {
        res.json(posts);
    })
    .catch(function (error) {
        res.json({message: error})
    });
};
module.exports.create = function (req, res) {
    var title = req.body.title;
    var desc = req.body.desc;
    var content = req.body.content;
    var slug = slugify(req.body.title).toLowerCase();
    var thumbnail = req.body.thumbnail;
    var about = req.body.about;
    var contact = req.body.contact;
    var CategoryId = req.body.CategoryId;
    var UserId = req.body.UserId;

    return PostService.createPost(title, desc, content, slug, thumbnail, about, contact, CategoryId, UserId)
    .then(function (posts) {
        res.json({message: 'Create success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });    
    // return UserService.createUser(username, password, avatar, fullname, dob, address, email, gender, phone, desc, roleId)
    // .then(function (users) {
    //     res.json(users);
    // })
    // .catch(function (error) {
    //     res.json({message: error})
    // });

};


module.exports.update = function (req, res) {
    var title = req.body.title;
    var desc = req.body.desc;
    var content = req.body.content;
    var slug = slugify(req.body.title).toLowerCase();
    var status = req.body.status;
    var thumbnail = req.body.thumbnail;
    var about = req.body.about;
    var contact = req.body.contact;
    var CategoryId = req.body.CategoryId;
    var UserId = req.body.UserId;
    return PostService.updatePost(title, desc, content, slug, status, thumbnail, about, contact, CategoryId, UserId)
    .then(function (users) {     
        res.status(200).json({message: 'Updated user success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });    
};

module.exports.delete = function (req, res) {
    var id = req.params.id;
    return PostService.deletePost(id)
    .then(function (users) {
        res.json({message: 'Deleted post success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });  
}; 
