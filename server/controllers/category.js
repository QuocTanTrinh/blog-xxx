var CategoryService = require('../services/category');
const slugify = require("slugify");
module.exports.getList = function (req, res) {
    var page = req.query.page || 1;
    return CategoryService.getList(page)
        .then(function (categories) {
            res.json(categories);
        })
        .catch(function (error) {
            res.json({message: error})
        });
};

module.exports.get = function (req, res) {
    var slug = req.params.slug;
    return CategoryService.getCategoryDetail(slug)
    .then(function (categories) {
        res.json(categories);
    })
    .catch(function (error) {
        res.json({message: error})
    });
};

module.exports.create = function (req, res) {
    var title = req.body.title;
    var desc = req.body.desc;
    var slug = slugify(req.body.title).toLowerCase();
    return CategoryService.createCategory(title, desc, slug)
    .then(function (categories) {
        res.json({message: 'Create success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });
};

module.exports.update = function (req, res) {
    var title = req.body.title;
    var desc = req.body.desc;
    var slug = slugify(req.body.title).toLowerCase();
    var id = req.body.id;
    return CategoryService.updateCategory(title, desc, slug, id)
    .then(function (categories) {
        //res.redirect("/" + slugify(req.body.title).toLowerCase());
        res.json({message: 'update success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });   
};

module.exports.delete = function (req, res) {
    var slug = req.params.slug;
    return CategoryService.deleteCategory(slug)
    .then(function (categories) {
        res.json(categories);
    })
    .catch(function (error) {
        res.json({message: error})
    });  
};