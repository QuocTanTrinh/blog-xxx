const ResourceService = require('../services/resource');
module.exports.getList = function (req, res) {
    var page = req.query.page || 1;
    return ResourceService.getList(page)
        .then(function (resources) {
            res.json(resources);
        })
        .catch(function (error) {
            res.json({message: error})
        });
};

module.exports.get = function (req, res) {
    var slug = req.params.slug;
    return ResourceService.getResourceDetail(slug)
    .then(function (resources) {
        res.json(resources);
    })
    .catch(function (error) {
        res.json({message: error})
    });
};

module.exports.create = function (req, res) {
    var desc = req.body.desc;
    var url = req.body.url;
    var path = req.body.path;
    var file_type = req.body.file_type;
    var extension = req.body.extension;
    return ResourceService.createResource(desc, url, path, file_type, extension)
    .then(function (resources) {
        res.json({message: 'Create success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });
};

module.exports.update = function (req, res) {
    var title = req.body.title;
    var desc = req.body.desc;
    var id = req.body.id;
    return CategoryService.updateResource(desc, url, path, file_type, extension, id)
    .then(function (resources) {
        res.json({message: 'update success'});
    })
    .catch(function (error) {
        res.json({message: error})
    });   
};

module.exports.delete = function (req, res) {
    var id = req.params.id;
    return CategoryService.deleteResource(id)
    .then(function (resources) {
        res.json(resources);
    })
    .catch(function (error) {
        res.json({message: error})
    });  
};