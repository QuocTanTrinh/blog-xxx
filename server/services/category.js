var CategoryModel = require('../models').Category;

module.exports.getList = function (page, perPage = 10) {
    return CategoryModel.findAll({offset: (page - 1) * perPage, limit: perPage});
};

module.exports.getCategoryDetail = function(slug){
    return CategoryModel.findOne({where:{slug: slug}});
}

module.exports.createCategory = function(title, desc, slug){
    return CategoryModel.findOrCreate({where: {title: title}, defaults:{desc: desc, slug: slug}})
                    .then(([categories, created])=>{
                        console.log(categories.get({
                            plain: true
                        }))
                        console.log(created)
                    });
};

module.exports.deleteCategory = function(slug){
        return CategoryModel.destroy({
            where: {slug: slug}}
        )
};

module.exports.updateCategory = function(title, desc, slug){
    return CategoryModel.update({
        title, 
        desc
    },
        {
            returning: true, 
            where:{
                slug
            }
        })
        .then(function([ rowsUpdate, [updatedCategory] ]) {
            res.json(updatedCategory)
          })  
}
