var UserModel = require('../models').User;
const jwtKey = require('../config/config');
var jwt = require('jsonwebtoken');
module.exports.generateToken = function(username, password) {
    return new Promise(function (resolve, reject) {
        UserModel.findOne({where: {username: username}})
            .then(function(users) {
                if (users.password !== password) {
                    reject({message: 'Username and password do not match'});
                    return;
                }

                var token = jwt.sign(
                   {
                        id: users.id,
                        username: users.username,
                    },
                     jwtKey.development.jwt_secret_key,
                    // "tan",

                    {
                        expiresIn: "1h"
                    }
                );
                resolve({token: token});
            })
            .catch(function (error) {
                reject({message: 'User not found'});
            });
    });
};

module.exports.verify = function (accessToken) {
    return new Promise(function (resolve, reject) {
        jwt.verify(accessToken, jwtKey.jwt_secret_key, function(error, decoded) {
            if (error) {
                reject(error);
                return;
            }
            resolve(decoded);
        });
    });
};