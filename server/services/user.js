var UserModel = require('../models').User;

module.exports.getList = function (page, perPage = 10) {
    return UserModel.findAll({offset: (page - 1) * perPage, limit: perPage});
};
module.exports.getUserDetail = function(userId){
    return UserModel.findByPk(userId);
};

module.exports.createUser = function (username, password, avatar, fullname, dob, address, email, gender, phone, desc, roleId) {
        return UserModel.findOrCreate({where: {username: username}, defaults:{password: password, fullname: fullname, dob: dob, address: address,
        email: email, gender: gender, phone: phone, desc: desc, RoleId: roleId}})
                        .then(([users, created])=>{
                            console.log(users.get({
                                plain: true
                            }))
                            console.log(created)
                        });        
    
};
module.exports.updateUser = function(username, password, avatar, fullname, dob, address, email, gender, phone, desc){
    return UserModel.update({password: password, fullname: fullname, dob: dob, address: address,
        email: email, gender: gender, phone: phone, desc: desc},
        {returning: true, where:{username: username}})
        .then(function([ rowsUpdate, [updatedUser] ]) {
            res.json(updatedUser)
          })        
};
module.exports.deleteUser = function(userId){
    return UserModel.destroy({
        where: {id: userId}}
    )
}; 