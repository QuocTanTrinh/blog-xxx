var PostModel = require('../models').Post;

module.exports.getList = function (page, perPage = 10) {
    return PostModel.findAll({offset: (page - 1) * perPage, limit: perPage});
};
module.exports.getPostDetail = function(postId){
    return PostModel.findByPk(postId);
};

module.exports.createPost = function (title, desc, content, slug, thumbnail, about, contact, CategoryId, UserId) {
        return PostModel.findOrCreate({where: {slug: slug}, defaults:{title: title, desc: desc, content: content, views: 0, slug: slug,
        status: 1, thumbnail: thumbnail, about: about, contact: contact, CategoryId: CategoryId, UserId: UserId}})
                        .then(([posts, created])=>{
                            console.log(posts.get({
                                plain: true
                            }))
                            console.log(created)
                        });        
    
};
module.exports.updatePost = function(title, desc, content, slug, status, thumbnail, about, contact, CategoryId, UserId){
    return PostModel.update({title: title, desc: desc, content: content, slug: slug,
        thumbnail: thumbnail, about: about, contact: contact, CategoryId: CategoryId},
        {returning: true, where:{slug: slug, UserId: UserId}})
        .then(function([ rowsUpdate, [updatedPost] ]) {
            res.json(updatedPost)
          })        
};
module.exports.deletePost = function(postId){
    return PostModel.destroy({
        where: {id: postId}}
    )
}; 