var RoleModel = require('../models').Role;

module.exports.getList = function (page, perPage = 10) {
    return RoleModel.findAll({offset: (page - 1) * perPage, limit: perPage});
};

module.exports.getRoleDetail = function(roleId){
    return RoleModel.findByPk(roleId);
}

module.exports.createRole = function(name, desc){
    return RoleModel.findOrCreate({where: {name: name}, defaults:{desc: desc}})
                    .then(([roles, created])=>{
                        console.log(roles.get({
                            plain: true
                        }))
                        console.log(created)
                    });
};

module.exports.deleteRole = function(roleId){
        return RoleModel.destroy({
            where: {id: roleId}}
        )
};

module.exports.updateRole = function(roleId, name, desc){
    return RoleModel.update({name: name, desc: desc},
        {returning: true, where:{id: roleId}})
        .then(function([ rowsUpdate, [updatedRole] ]) {
            res.json(updatedRole)
          })  
}
module.exports.getRoleId = function(roleName) {
    return RoleModel.findOne({
        where: {name: roleName},
        attributes: ['id']
    });
    
};
