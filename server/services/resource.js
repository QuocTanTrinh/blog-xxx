var ResourceModel = require('../models').Resource;

module.exports.getList = function (page, perPage = 10) {
    return ResourceModel.findAll({offset: (page - 1) * perPage, limit: perPage});
};

module.exports.getResourceDetail = function(slug){
    return ResourceModel.findOne({where:{slug: slug}});
}

module.exports.createResource = function(desc, url, path, file_type, extension){
    return ResourceModel.findOrCreate({where: {url: url}, defaults:{desc: desc, path: path, file_type: file_type, extension: extension}})
                    .then(([resources, created])=>{
                        console.log(resources.get({
                            plain: true
                        }))
                        console.log(created)
                    });
};

module.exports.deleteResource = function(id){
        return ResourceModel.destroy({
            where: {id: id}}
        )
};

module.exports.updateResource = function(desc, url, path, file_type, extension, id){
    return ResourceModel.update({
        desc, url, path, file_type, extension
    },
        {
            returning: true, 
            where:{
                id
            }
        })
        .then(function([ rowsUpdate, [updatedResource] ]) {
            res.json(updatedCategory)
          })  
}
