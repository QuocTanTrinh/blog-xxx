'use strict';
module.exports = (sequelize, DataTypes) => {
  const Resource = sequelize.define('Resource', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      autoIncrement: false,
    },    
    desc: DataTypes.STRING,
    url: DataTypes.STRING,
    path: DataTypes.STRING,
    file_type: DataTypes.ENUM('0', '1'),
    extension: DataTypes.ENUM('jpg', 'mp4', 'png', 'jpeg')
  }, {});
  Resource.associate = function(models) {
    // associations can be defined here
      Resource.belongsToMany(models.Post, {
      through: models.Resource_Post,
      as: 'posts',
      foreignKey: 'resource_id'
      });
      Resource.belongsTo(models.User);  
  };
  return Resource;
};