'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define('Tag', {
  	id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      autoIncrement: false,
    },  	
    name: DataTypes.STRING
  }, {});
  Tag.associate = function(models) {
    // associations can be defined here
      Tag.belongsToMany(models.Post, {
      through: models.Tag_Post,
      as: 'posts',
      foreignKey: 'tag_id'
    });     
  };
  return Tag;
};