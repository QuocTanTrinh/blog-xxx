'use strict';
module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      autoIncrement: false,
    },    
    title: DataTypes.STRING, //title, desc, content, views, slug, status, thumbnail, about, contact
    desc: DataTypes.STRING,
    content: DataTypes.TEXT,
    views: DataTypes.INTEGER,
    slug: DataTypes.STRING,
    status: DataTypes.ENUM('0','1','2'),   //0: chờ xét duyệt, 1: post xuất hiện trên homepage, 2:admin từ chối up bài
    thumbnail: DataTypes.UUIDV4,
    about: DataTypes.STRING,
    contact: DataTypes.STRING
  }, {});
  Post.associate = function(models) {
    // associations can be defined here
      Post.belongsToMany(models.Resource, {
        through: models.Resource_Post,
        as: 'resources',
        foreignKey: 'post_id'
      });
      Post.belongsToMany(models.Tag, {
        through: models.Tag_Post,
        as: 'tags',
        foreignKey: 'post_id'
      });
      Post.belongsTo(models.User);
      Post.belongsTo(models.Category);
      Post.hasMany(models.Comment);

  };
  return Post;
};