'use strict';
module.exports = (sequelize, DataTypes) => {
  const Resource_Post = sequelize.define('Resource_Post', {
    resource_id: {
    	type: DataTypes.UUID,
    	allowNull: false,
    	primaryKey: true,
    	references: {
    		model: 'Resource',
    		key: 'id'
    	}
    },
    post_id: {
    	type: DataTypes.UUID,
    	allowNull: false,
    	primaryKey: true,
    	references: {
    		model: 'Post',
    		key: 'id'
    	}
    }
	}, {});
  Resource_Post.associate = function(models) {
    // associations can be defined here
    
  };
  return Resource_Post;
};