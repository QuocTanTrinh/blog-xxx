'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tag_Post = sequelize.define('Tag_Post', {
    tag_id: {
    	type: DataTypes.UUID,
    	allowNull: false,
    	primaryKey: true,
    	references: {
    		model: 'Tag',
    		key: 'id'
    	}
    },
    post_id: {
    	type: DataTypes.UUID,
    	allowNull: false,
    	primaryKey: true,
    	references: {
    		model: 'Post',
    		key: 'id'
    	}
    }
	}, {});
  Tag_Post.associate = function(models) {
    // associations can be defined here
  };
  return Tag_Post;
};