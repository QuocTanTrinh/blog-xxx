'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
  	id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      autoIncrement: false,
    },  	
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.UUID,
    fullname: DataTypes.STRING,
    dob: DataTypes.DATEONLY,
    address: DataTypes.STRING,
    email: DataTypes.STRING,
    gender: DataTypes.ENUM('0','1'),
    phone: DataTypes.STRING,
    desc: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.belongsTo(models.Role);
    User.hasMany(models.Resource);
    User.hasMany(models.Post);
    User.hasMany(models.Comment);
  };
  return User;
};
