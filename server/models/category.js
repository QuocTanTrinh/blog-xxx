'use strict';
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
  	id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      autoIncrement: false,
    },
    title: DataTypes.STRING,
    desc: DataTypes.STRING,
    slug: DataTypes.STRING
  }, {});
  Category.associate = function(models) {
    // associations can be defined here
    Category.hasMany(models.Post);
  };
  return Category;
};