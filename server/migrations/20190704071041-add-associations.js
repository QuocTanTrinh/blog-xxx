'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn(
        'posts',
        'CategoryId',
        {
          type: Sequelize.UUID,
          references: {
            model: 'categories',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        }
        )
      .then(() => {
            return queryInterface.addColumn(
              'posts',
              'UserId',
              {
                type: Sequelize.UUID,
                references:{
                  model: 'users',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
              );
          })
      .then(() => {
            return queryInterface.addColumn(
              'users',
              'RoleId',
              {
                type: Sequelize.UUID,
                references:{
                  model: 'roles',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
              );
          })
          .then(() => {
            return queryInterface.addColumn(
              'comments',
              'UserId',
              {
                type: Sequelize.UUID,
                references:{
                  model: 'users',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
              );
          })
          .then(() => {
            return queryInterface.addColumn(
              'comments',
              'PostId',
              {
                type: Sequelize.UUID,
                references:{
                  model: 'posts',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
              );
          })
          .then(() => {
            return queryInterface.addColumn(
              'resources',
              'UserId',
              {
                type: Sequelize.UUID,
                references:{
                  model: 'users',
                  key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
              }
              );
          });                    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
        'posts',
        'CategoryId'
      )
      .then(()=>{
        return queryInterface.removeColumn(
            'posts',
            'UserId'
          );
      })
      .then(()=>{
        return queryInterface.removeColumn(
            'users',
            'RoleId'
          );        
      })
      .then(()=>{
        return queryInterface.removeColumn(
            'comments',
            'UserId'
          );
      })
      .then(()=>{
        return queryInterface.removeColumn(
            'comments',
            'PostId'
          );
      })  
      .then(()=>{
        return queryInterface.removeColumn(
            'resources',
            'UserId'
          );
      });                          
  }
};