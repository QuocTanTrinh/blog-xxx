'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Resources', {
      id: {
        allowNull: false,
        autoIncrement: false,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        type: Sequelize.UUID
      },
      desc: {
        type: Sequelize.STRING
      },
      url: {
        type: Sequelize.STRING
      },
      path: {
        type: Sequelize.STRING
      },
      file_type: {
        type: Sequelize.ENUM('0', '1')
      },
      extension: {
        type: Sequelize.ENUM('jpg', 'mp4', 'png', 'jpeg')
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Resources');
  }
};